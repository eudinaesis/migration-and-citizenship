---
title: Migration and Citizenship
author: Peter Northup
date: 22.4.2020
lang: en
width: 1920
height: 1200
theme: black
bibliography: /Users/pnorthup/Documents/bibs/all.json
nocite: |
	@leydetCitizenship2017, @brubakerMigrationMembershipModern2010, @baubockDemocraticInclusion2018, @baubockCitizenshipMigrationConcepts2006, @abizadehReviewSymposiumEthics2015, @songSignificanceTerritorialPresence2016
---

# Intro: a stranger in one's own country

## [Advanced Chemistry, Fremd im eigenen Land (1992)](https://www.youtube.com/watch?v=hU5j_RqUjas) {data-background-image="media/advanced-chemistry-fiel.png"}

Advanced Chemistry was a pioneering socially-conscious German hip-hop group from Heidelberg, active in the late 80s and early-mid 90s. Its members all had migrant backgrounds (Italy, Haiti, and Ghana, respectively).

What is the relationship between migration, formal citizenship, and social belonging in this video?

## "Fremd im eigenen Land": discuss!

Ich habe einen grünen Pass mit 'nem goldenen Adler drauf /
Dies bedingt, dass ich mir oft die Haare rauf /
Jetzt mal ohne Spaß: Ärger hab' ich zuhauf /
Obwohl ich langsam Auto fahre und niemals sauf

(All das Gerede von europäischem Zusammenschluss) /
Fahr' ich zur Grenze mit dem Zug oder einem Bus /
Frag' ich mich, warum ich der Einzige bin, der sich ausweisen muss /
Identität beweisen muss!

(Ist es so ungewöhnlich, wenn ein Afro-Deutscher seine Sprache spricht) /
Und nicht so blass ist im Gesicht? /
Das Problem sind die Ideen im System /
(Ein echter Deutscher muss auch richtig deutsch aussehen)

Blaue Augen, blondes Haar, keine Gefahr /
Gab's da nicht 'ne Zeit wo's schon mal so war? /
"Gehst du mal später zurück in deine Heimat?" /
Wohin? nach Heidelberg? wo ich ein Heim hab?

"Nein du weisst, was ich mein..." /
Komm lass es sein, ich kenn diese Fragen, seitdem ich klein /
Bin, in diesem Land vor zwei Jahrzehnten gebor'n /
Doch frag' ich mich manchmal: Was hab ich hier verlor'n?

Ignorantes Geschwätz, ohne End' /
Dumme Sprüche, die man bereits alle kennt /
"Eh, bist du Amerikaner oder kommste aus Afrika?" /
(Noch ein Kommentar über mein Haar, was ist daran so sonderbar?)

"Ach du bist Deutscher, komm erzähl kein Scheiß!" /
(Du willst den Beweis? Hier ist mein Ausweis /
Gestatten Sie, mein Name ist Frederik Hahn /
Ich wurde hier geboren, doch wahrscheinlich sieht man es mir nicht an

Ich bin kein Ausländer, Aussiedler, Tourist, Immigrant) /
Sondern deutscher Staatsbürger und komme zufällig aus diesem Land

## Citizenship & belonging

- Formal citizenship brings demands for full "social citizenship" & inclusion
- But this goes other way, too: dominant groups may refuse to recognize the "real" citizenship of those who don't "really" belong (*"Ein echter Deutscher muss auch richtig deutsch aussehen"*)
- Migration highlights this tension, but it also applies to others who don't fit the idealized image of a citizen & community member
    - Working class, indigenous or historical minorities, women

## What's distinctive about migration?

::: incremental

- Many do *not* have formal citizenship where they live
- Key questions: does the migrant belong? How much? Enough for citizenship?
- What is the relationship between these questions and the *right to migrate*?

:::

# Migrants & citizenship as equal rights

## Which rights are reserved for citizens?

Varies, but often:

- Land/property ownership
- Certain jobs: police, judges, armed forces
- Unconditional right to enter & stay in country
- Labor market
- Social welfare

## Why these? Trust, belonging, solidarity

- Scarce resources (jobs, social welfare)
    - "We all have a right to these, because we're in this together"
- Positions of authority & power
    - "We can all be trusted to exercise this power, because we're basically on the same team"

## "Citizenship as rights" tied up with "citizenship as belonging"

# Migrants & citizenship as political participation

## What justifies political rights at all?

::: incremental

- Self-determination: members of a collective community have an interest in shaping its destiny over time
- Freedom: everyone subject to law should have a say in what the law is
- Equality: because everyone's interests are of equal moral worth, everyone should have an *equal* say in political decisions
- Wisdom: because no one has a monopoly on wisdom, we should broaden the circle of participation as widely as possible
- How do these arguments apply to migrants?

:::

## Which justifications apply differently to migrants?

::: incremental

- Wisdom
    - Are migrants somehow unfit for political participation?
- Equality
    - Do migrants' interests have equal moral worth?
- Self-determination
    - Are migrants "members of the collective community"?
- Freedom
    - Also subject to coercion, but does consent matter?

:::



# The false promise of nationalist citizenship

## Nationalism and the three aspects of citizenship

- Identity: residents = citizens = members of the nation; everyone inside (and no one outside) belongs
- Equal rights: because the nation is a community with a common culture, history, and ethics, there is enough social trust to universalize even rights to scare goods or positions of power
- Equal participation: nations have a right to self-determination; the common interests within the nation mean it is "safe" to have equal political rights

## So what's the problem?

- Nationalism was never an empirical reality, except perhaps in the aftermath of WWII--and only then due to genocide and massive forced population expulsions
- The assumption of internal commonality (and external difference) is unwarranted, and often an excuse for suppression of internal diversity
- Migration has always been a normal part of human history: if nationalist conceptions of citizenship can't deal with it, so much the worse for nationalism

## The way forward

- There are smart arguments about, e.g., why long-term residents deserve citizenship or participation rights (see literature slide)…
- But ultimately the heart of the debate goes to identity and belonging

![FPÖ Steiermark election poster, 2015](media/fpoe-fremd-im-eigenen-land.jpg)

# Sources

## Media / credits

Advanced Chemistry. 1992. _Fremd Im Eigenen Land: Music Video_. [https://www.youtube.com/watch?v=hU5j_RqUjas](https://www.youtube.com/watch?v=hU5j_RqUjas).

“Lyrics: ‘Advanced Chemistry – Fremd Im Eigenen Land.’” Genius.com. Accessed April 22, 2020. [https://genius.com/Advanced-chemistry-fremd-im-eigenen-land-lyrics](https://genius.com/Advanced-chemistry-fremd-im-eigenen-land-lyrics).

_Österreich.At_. 2015\. “FPÖ: Wahlschlacht Gegen Die Ausländer,” May 30, 2015. [https://www.österreich.at/politik/steiermark-wahl/fpoe-wahlschlacht-gegen-die-auslaender/190509971](https://www.österreich.at/politik/steiermark-wahl/fpoe-wahlschlacht-gegen-die-auslaender/190509971).

## Literature

