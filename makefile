# index.html is normal revealjs
# [filename].html is self-contained revealjs
# .pdf is self-contained revealjs
# make [all] does all 3, copies the media to public/media

all: index.html $(patsubst %.md,%.html,$(wildcard *.md)) $(patsubst %.md,%.pdf,$(wildcard *.md))

index.html: *.md
	pandoc --citeproc --standalone -t revealjs $< -o public/$@
	cp -r media public/

%.html: %.md
	pandoc --citeproc --standalone --self-contained -t revealjs $< -o public/$@

%.pdf: %.md
	pandoc --citeproc --standalone --self-contained -t pdf $< -o public/$@
